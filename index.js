'use strict';

const {FB, FacebookApiException} = require('fb');

const getRatings = exports.getRatings = (accessToken, fbPageId, data, url) => new Promise((resolve, reject) => {
  FB.setAccessToken(accessToken);
  if(!url) {
    url = `${fbPageId}/ratings?limit=1000&fields=open_graph_story,reviewer,rating,recommendation_type,review_text,created_time`;
  } else {
    url = url.match(/\/v[\d\.]+\/(.*)/)[1];
  }

  FB.api(url, function (res) {
    if(!res || res.error) {
     reject(!res ? 'error occurred' : res.error);
     return;
    }

    data = [...data, ...res.data];

    if(res.paging && res.paging.next) {
      getRatings(accessToken, fbPageId, data, res.paging.next);
    } else {
      resolve(data);
    }
  });
});

const filterRatings = exports.filterRatings = ratings => {
  return ratings.filter(
      elem => elem.rating === 5 ||
        (elem.recommendation_type === 'positive' &&
        elem.rating === undefined)
    );
};
