# fb-ratings

NodeJS library for obtaining your facebook page reviews.

## Methods

### getRatings

#### output

A promise that returns an array of objects that represent each rating if resolved, or an error string if rejected.

#### arguments

* **accessToken**: a valid Facebook API *access token*. It has to be a Page Access Token with *manage_pages* permission.
* **fbPageId**: the Facebook Page **ID** (you can get that number at `https://www.facebook.com/pg/<your_facebook_page_name>/about`).
* **data**: the array where you want to append the new ratings that will be obtained (you can just create a new array with `[]` and pass it)
* **url** (optional): if you want to change the URL that the method will use to query Facebook's API

**Important**: this method is recursive. If there are several pages being returned by the API, it will query them recursively.

If there's a lot of pages, this method could take a while.

For more information: [https://developers.facebook.com/docs/graph-api/reference/page/ratings/](https://developers.facebook.com/docs/graph-api/reference/page/ratings/).

### filterRatings

#### output

Array of ratings containing only those who are 5-stars (old facebook rating system) or doesn't have a star rating, but a positive recommendation (new facebook rating system)

#### arguments

* **ratings**: array of ratings (you can pass the result of calling getRatings)

## Why?

Because sometimes you need those "sweet" top reviews as social proof for marketing campaigns, sales processes, promotion, etc.

And this library will help you get them fast and easy.
